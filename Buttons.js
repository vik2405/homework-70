import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

const Buttons = props => {
    return (
        <TouchableOpacity style={[styles.button]} onPress={props.pressed}>
        <View>
        <Text style={styles.text}>{props.text}</Text>
        </View>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    button: {
      height: '30%',
      width: '20%',   
      alignItems: 'center',
      borderColor: 'black',
    },
    text: {
        fontSize:60,
        color: 'red',
    }
  });
  
  export default Buttons;