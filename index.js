import React from 'react';

import { AppRegistry } from 'react-native';
import App from './App';

import {createStore} from 'redux';
import reducer from './src/store/reducer';
import {Provider} from 'react-redux';
const store = createStore(reducer);
const Application = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent('Hw70', () => Application);
