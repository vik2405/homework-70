import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Buttons from './Buttons';
import {connect} from 'react-redux'
import actionTypes from './src/store/actionTypes';


 class App extends Component {
  render() {
    return (
      <View style={styles.container}>
      <View style={[styles.row, styles.output]}>
        <Text style={{fontSize: 80,marginBottom:80,marginRight:50,fontWeight: 'bold',}}>{this.props.scoreboard}</Text>
      </View>
      <View style={styles.row}>
      <Buttons text='1' pressed={() => this.props.addNumber('1')} />
      <Buttons text='2' pressed={() => this.props.addNumber('2')} />
      <Buttons text='3' pressed={() => this.props.addNumber('3')} />
      <Buttons text='4' pressed={() => this.props.addNumber('4')} />
      <Buttons text='5' pressed={() => this.props.addNumber('5')} />
      <Buttons text='6' pressed={() => this.props.addNumber('6')} />
      <Buttons text='7' pressed={() => this.props.addNumber('7')} />
      <Buttons text='8' pressed={() => this.props.addNumber('8')} />
      <Buttons text='9' pressed={() => this.props.addNumber('9')} />
      <Buttons text='0' pressed={() => this.props.addNumber('0')} />
      <Buttons text='+' pressed={() => this.props.addNumber('+')} />
      <Buttons text='-' pressed={() => this.props.addNumber('-')} />
      <Buttons text='E' pressed={() => this.props.resultNumber('')} />
      <Buttons text='C' pressed={() => this.props.delNumber('')} />
      <Buttons text='R' pressed={() => this.props.remNumber('')} />
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  row: {
    flex: 1,
    width: '100%',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  output: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  }
});


const mapStateToProps = state => {
  return {
      scoreboard: state.counter,
  }
};
const mapDispatchToProps = dispatch => {
  return {
      addNumber: (number) => dispatch({type: actionTypes.NUMBER_TYPE, number}),
      delNumber: (number) => dispatch({type: actionTypes.DEL_NUMBER, number}),
      resultNumber:(number) => dispatch({type: actionTypes.RESULT_NUMBER, number}),
      remNumber: (number) => dispatch({type: actionTypes.REM_NUMBER, number})
  };
};

   export default connect(mapStateToProps,mapDispatchToProps)(App);